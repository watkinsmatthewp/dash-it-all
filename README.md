# Dash It All!

## What is Dash It All?

This is a C# .NET Core app that lets your sniff network packets and trigger web requests based on configurable rules.

## Wait, but why?

This past Amazon Prime Day, I bought 20 Dash Buttons for $1 each with the intent to turn them into smart buttons so I can hijack them to turn on my AC, record work hours, and basically do whatever I want. I liked Ted Benson's [article on hacking the Amazon Dash Button](https://blog.cloudstitch.com/how-i-hacked-amazon-s-5-wifi-button-to-track-baby-data-794214b0bdd8), but I don't want to install Python, and I want to add a management layer and share it with the world. So this project was born.

This is currently running on my Raspberry Pi at home and works well.